# KiCadOpenSourceProjectCollection

#### 介绍

KiCad 开源项目合集

#### 软件架构

本仓库使用 `git submodule` 方式收集 KiCad 开源项目合集

#### 安装教程

1. 克隆本仓库

```bash
git clone https://gitee.com/KiCAD-CN/KiCadOpenSourceProjectCollection.git
```

2. 使用子模块仓库

```bash
// 初始化子模块仓库
git submodule init 

// 更新子模块仓库
git submodule update
```

或：

```bash
// 初始化和更新子模块仓库组合命令
git submodule update --init --recursive
```

#### 使用说明

1. [dvk-mx8m-bsb](dvk-mx8m-bsb) 仓库地址：https://source.puri.sm/Librem5/dvk-mx8m-bsb


> # dvk-mx8m-bsb
> 
> Librem 5 Developer Kit using EmCraft's i.MX 8M System-on-Module.
> 
> KiCad v5.0.0 has been used to create this design.
> 
> ## License
> 
> dvk-mx8m-bsb is [licensed under the GNU GPLv3+](https://source.puri.sm/Librem5/dvk-mx8m-bsb/blob/master/LICENSE).


2. [usbarmory](usbarmory) 仓库地址：https://github.com/f-secure-foundry/usbarmory/

> # usbarmory
> Introduction
> ============
> 
> USB armory | https://github.com/f-secure-foundry/usbarmory  
> Copyright (c) F-Secure Corporation
> 
> The USB armory from [F-Secure Foundry](https://foundry.f-secure.com) is an open
> source hardware design, implementing a flash drive sized computer.
> 
> This repository is aimed towards developers, if you wish to purchase a USB
> armory board please see the [USB armory project page](https://www.f-secure.com/en/consulting/foundry/usb-armory).
> 
> Authors
> =======
> 
> Andrea Barisani  
> andrea.barisani@f-secure.com | andrea@inversepath.com  
> 
> Andrej Rosano  
> andrej.rosano@f-secure.com   | andrej@inversepath.com  
> 
> Daniele Bianco  
> daniele.bianco@f-secure.com   | daniele@inversepath.com  
> 
> Documentation
> =============
> 
> The main documentation can be found on the
> [project wiki](https://github.com/f-secure-foundry/usbarmory/wiki).
> 
> Board revisions
> ===============
> 
> * [USB armory Mk II](https://github.com/f-secure-foundry/usbarmory/wiki/Mk-II-Introduction)
> 
> ![Mk II Top](https://github.com/f-secure-foundry/usbarmory/wiki/images/armory-mark-two-top.png)
> ![Mk II Bottom](https://github.com/f-secure-foundry/usbarmory/wiki/images/armory-mark-two-bottom.png)
> 
> * USB armory Mk I: first production release.
> 
> ![USB armory Mk I](https://github.com/f-secure-foundry/usbarmory/wiki/images/armory-mark-one.png)
> 
> License
> =======
> 
> USB armory | https://github.com/f-secure-foundry/usbarmory  
> Copyright (c) F-Secure Corporation
> 
> This is an open hardware design licensed under the terms of the CERN Open
> Hardware Licence (OHL) v1.2.
> 
> You may redistribute and modify this documentation under the terms of the CERN
> OHL v.1.2 (http://ohwr.org/cernohl). This documentation is distributed WITHOUT
> ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY
> QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN OHL v.1.2 for
> applicable conditions.


3. [H7PI](https://gitee.com/htctek/H7PI) 仓库地址：https://gitee.com/htctek/H7PI
> # H7PI
> 简介
> ============
> H7PI基于STM32H750VBT6, 设计初衷是为了做一个通用平台，在这个设计上，按照一个场景一个
> 应用的模式扩展出其他扩展功能，同时实现如代码复用一般的硬件复用。板载8M QSPI Flash程序空间，8M SPI Flasn文件空间，支持扩展SD卡，支持扩展LCD.[参考说明](https://pinno.cc/2020/02/28/H7PI/)
> 
> #### KiCad v5.1.5 has been used to create this design.    
> ![H7PI](https://pinno.cc/2020/02/28/H7PI/wp.jpg)
> 
> license
> ============
> 
> H7PI is [licensed under the BSD 3-Clause](https://gitee.com/htctek/H7PI/blob/master/LICENSE).


4. [ESP32PI](https://gitee.com/htctek/ESP32PI) 仓库地址：https://gitee.com/htctek/ESP32PI
> # H7PI
> 简介
> ============
> ESP32PI基于ESP32-PICO-D4, 为快速开发wifi，ble应用提供方案验证的可能
> * 板载CP2102，可通过USB转串口直接更新固件，无需手动按按键
> * 板载Micro SD Card接口，扩展文件系统
> * 板载天线，可直接链接WiFi和蓝牙，无需额外购买天线
> * 板载PSRAM，增加应用内存空间
> * 所有IO引出，方便复用，可扩展多种功能
> [参考说明](https://pinno.cc/2020/04/07/ESP32PI/)
> 
> #### KiCad v5.1.5 has been used to create this design.    
> ![ESP32PI](https://pinno.cc/2020/04/07/ESP32PI/ESP32PI.JPG)
> 
> license
> ============
> 
> ESP32PI is [licensed under the BSD 3-Clause](https://gitee.com/htctek/ESP32PI/blob/master/LICENSE).


#### 参与贡献

1.  Fork 本仓库

2.  新建 Feat_xxx 分支

3.  添加新的子模块仓库

```bash
// `URL` 为子模块的路径，`PATH` 为该子模块存储的目录路径
git submodule add [URL] [PATH]
```
例如：

```bash
// 仓库后缀的 `.git` 可加可不加
git submodule add https://source.puri.sm/Librem5/dvk-mx8m-bsb
```

4. 从子模块的远端仓库更新并合并到本仓库

```bash
// 从子模块远端仓库更新并合并
git submodule update --remote --merge
```
5.  提交子模块的更改

```bash
// 提交修改
// -s : 在提交日志消息的末尾添加提交人签名行。
// 签约的含义取决于项目，但它通常证明提交者有权在相同的许可证下提交此作品，
// 并同意开发者原产地证书(有关更多信息，请参阅 http://developercertificate.org/)。

git commit -sa 
```

修改本仓库的 `README` 自述文件，增加对新增子模块仓库的描述。
```bash
// 使用文本编辑器编辑 `README.md`, 如：
// vim notepad gedit notepad++ nano emacs ...
vim README.md
// vim 批量处理 markdown(md) 语法引用操作 >
// 在 vim 编辑界面下，如给 usbarmory 批量添加引用符号 >
// : 编辑模式 54 开始行号 110 结束行号 s 搜索 ^ 行首 > 替换内容 g 替换
:54,110 s/^/> /g
// 修改完后记得保存退出，并清理文本编辑的缓存文件。
// 或者将缓存文件的格式添加到 `.gitignore` 文件中。


// 提交 README.md 修改 
git commit -am "此次修改的描述"
// 例如：
git commit -am "Add dvk-mx8m-bsb"
git commit -am "新增 dvk-mx8m-bsb"
git commit -am "Update dvk-mx8m-bsb"
git commit -am "更新 dvk-mx8m-bsb"
git commit -am "Delete dvk-mx8m-bsb"
git commit -am "删除 dvk-mx8m-bsb"
```

推送到自己的仓库
```bash
git push 
```
6.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
